package main

import ex "goLearning/exercises"

func main() {

	// ex.Variables()
	// ex.ControlFlow()
	// ex.GroupingData()
	ex.Structs()

	/* 		people := createSliceOfPerson()
	   		fmt.Println(people)

	   		for _, v := range people {
	   			fmt.Println(v.firstName, "is", v.age, "years old")
	   		} */

}
